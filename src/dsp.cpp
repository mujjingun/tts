#include "dsp.h"
#include "utils.h"
#include <iostream>

#define M_PI 3.14159265358979323846264338328L

// find fundamental period by autocorrelation
int fundamental_period(csig_t const& dft)
{
    // calculate autocorrelation
    csig_t acorr = dft * dft.apply(std::conj);
    ifft(acorr);
    
    // take real part and differentiate
    rsig_t racorr(dft.size() / 2);
    for(unsigned i = 1; i < dft.size() / 2; i++)
    {
        racorr[i] = acorr[i].real();
    }
    
    // extract f0 (find max)
    unsigned i = Fs / 500; // 500Hz
    unsigned to = Fs / 50; // 50Hz
    unsigned mi = i; // max
    for(; i < to; i++)
    {
        if(racorr[i] > racorr[mi]) mi = i;
    }
    
    if(racorr[mi] < 0.2) mi = 0;
    
    return mi;
}

// perform time domain pitch synchronous overlap add
rsig_t td_psola(rsig_t seg, std::valarray<int> peaks, int p)
{
    if(p == 0) return seg;
    
    rsig_t segc(seg.size());
    
    // find max index of windowed signal (fake epoch)
    int mi = 0; 
    for(unsigned i = 0; i < seg.size(); i++)
    {
        if(seg[i] > seg[mi]) mi = i;
    }
    mi %= p; // psola start position
    
    // create hann window of size 2p
    rsig_t win(seg.size());
    for(int i = 0; i < p * 2; i++)
    {
        win[i] = hann(p * 2, i);
    }
    
    // perform td-psola
    for(unsigned k = 0; k < seg.size(); k++)
    {
        if(peaks[k] == 1)
        {
            // nearest period start
            int np = int(floor(fpoint(k - mi) / p)) * p + mi - p;
            segc += (win.shift(-np) * seg).shift(np - k);
        }
    }
    
    return segc;
}

// Cooley-Tukey FFT (in-place, breadth-first, decimation-in-frequency)
// Better optimized but less intuitive
void fft(csig_t &x)
{
    // DFT
    unsigned N = x.size(), k = N;

    // if not power of two, do nothing
    if(N & (N - 1)) return;

    double thetaT = M_PI / N;
    complex phiT = complex(cos(thetaT), sin(thetaT)), T;
    while (k > 1)
    {
        unsigned n = k;
        k >>= 1;
        phiT = phiT * phiT;
        T = 1.0L;
        for (unsigned int l = 0; l < k; l++)
        {
            for (unsigned int a = l; a < N; a += n)
            {
                unsigned int b = a + k;
                complex t = x[a] - x[b];
                x[a] += x[b];
                x[b] = t * T;
            }
            T *= phiT;
        }
    }
    // Decimate
    unsigned int m = (unsigned int)log2(N);
    for (unsigned int a = 0; a < N; a++)
    {
        unsigned int b = a;
        // Reverse bits
        b = (((b & 0xaaaaaaaa) >> 1) | ((b & 0x55555555) << 1));
        b = (((b & 0xcccccccc) >> 2) | ((b & 0x33333333) << 2));
        b = (((b & 0xf0f0f0f0) >> 4) | ((b & 0x0f0f0f0f) << 4));
        b = (((b & 0xff00ff00) >> 8) | ((b & 0x00ff00ff) << 8));
        b = ((b >> 16) | (b << 16)) >> (32 - m);
        if (b > a)
        {
            complex t = x[a];
            x[a] = x[b];
            x[b] = t;
        }
    }
}

// inverse fft (in-place)
void ifft(csig_t& x)
{
    // conjugate the complex numbers
    x = x.apply(std::conj);

    // forward fft
    fft( x );

    // conjugate the complex numbers again
    x = x.apply(std::conj);

    // scale the numbers
    x /= x.size();
}
