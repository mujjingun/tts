#include <cmath> // fmin, fmax
#include <random>
#include "utils.h"

#define M_PI 3.14159265358979323846264338328L
#define M_2PI (2 * M_PI)

inline double angle_norm(double x);

std::default_random_engine noise_generator;
std::normal_distribution<fpoint> gauss_distribution(0, 1.0);

fpoint osc_noise()
{
    return clamp(-1, 1, gauss_distribution(noise_generator));
}

// sin(x) + sin(2*x) + ... + sin((n-1)*x)
fpoint sin_series(fpoint x, int n)
{
    x *= 0.5;
    fpoint s = sin(x);
    if(fabs(s) < 1e-12)
        return n * (n - 1) * s;
    return sin(n * x) * sin(x * (1 - n)) / s;
}

fpoint osc_blit(fpoint phase, fpoint harmonics)
{
    int ih = floor(harmonics);
    fpoint mu = harmonics - ih;
    ih += 1;
    fpoint w = 2 * M_PI * phase;
    return (sin_series(w, ih) + mu * sin(ih * w)) / harmonics;
}

// wrap to [-pi,pi]
inline double angle_norm(double x)
{
    x = fmod(x + M_PI, M_2PI);
    if (x < 0) x += M_2PI;
    return x - M_PI;
}

double phase_unwrap(double prev, double now)
{
    return prev + angle_norm(now - prev);
}

fpoint hann(int N, int i)
{
    return .5*(1 - cos(2 * M_PI * i / (N+1)));
}