#ifndef SYNTHESIZER_H
#define SYNTHESIZER_H

#include <vector>

#include "main.h"

class Synthesizer
{
public:
    Synthesizer();
    Synthesizer(std::vector<int> const& phonemes);

    void start(std::vector<int> const& phonemes);
    std::vector<float> generate_frame();
    bool has_ended();

protected:
    void phoneme_transition();
    fpoint gen_signal(fpoint freq, fpoint voice_level, fpoint noise_level);
    fpoint noise();

private:
    std::vector<int> phonemes;
    std::vector<fpoint> orig, orig2;
    rsig_t ova, ova2;
    rsig_t sig, sig2;
    std::valarray<int> peaks;

    int samp;
    int phoneme_idx;
    bool last_vocing;
    bool finished;

    fpoint last_phoneme_samp;
    fpoint last_period_samp;
    fpoint transition_start;
};

#endif // SYNTHESIZER_H
