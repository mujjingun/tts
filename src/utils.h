#ifndef FORMANTS_H_INCLUDED
#define FORMANTS_H_INCLUDED

#include "main.h"

inline fpoint clamp(fpoint s, fpoint e, fpoint x)
{
    return fmax(fmin(e, x), s);
}

inline fpoint linear(fpoint s, fpoint e, fpoint x, fpoint sx, fpoint ex)
{
    return (e - s) * clamp(0, 1, (x - sx) / (ex - sx)) + s;
}

fpoint osc_noise();
fpoint osc_blit(fpoint phase, fpoint harmonics);

double phase_unwrap(double prev, double now);

fpoint hann(int N, int i);

#endif // FORMANTS_H_INCLUDED
