#ifndef CEPSTRUM_H_INCLUDED
#define CEPSTRUM_H_INCLUDED

#include "main.h"

int fundamental_period(csig_t const& dft);
void fft(csig_t &x);
void ifft(csig_t &x);
rsig_t td_psola(rsig_t seg, std::valarray<int> peaks, int p);

#endif // CEPSTRUM_H_INCLUDED
