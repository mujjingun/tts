#ifndef MAIN_H_INCLUDED
#define MAIN_H_INCLUDED

#include <cstdint>
#include <vector>
#include <complex>
#include <valarray>

// double-precision floating point
typedef double fpoint;

// complex type alias
typedef std::complex<fpoint> complex;

// real signal type
typedef std::valarray<fpoint> rsig_t;

// complex signal type
typedef std::valarray<complex> csig_t;

// sampling rate
#define Fs fpoint(22050)

#endif // MAIN_H_INCLUDED
