#include <cstdlib>
#include <cmath>
#include <iostream>
#include <sstream>
#include <iomanip>

#include "main.h"
#include "synthesizer.h"
#include "soundio.h"
#include "utils.h"
#include "dsp.h"
#define M_PI 3.14159265358979323846264338328L

// FFT frame size
const int L = 1024;

// overlap factor
const int Ov = 8;

// step size
const int R = L / Ov;

// phoneme overlap size
const int POv = R * 8;

rsig_t window(L);

Synthesizer::Synthesizer():
    phonemes(),
    orig(POv, 0),
    orig2(),
    ova(L),
    ova2(L),
    sig(L),
    sig2(L),
    peaks(L),

    samp(0),
    phoneme_idx(0),
    last_vocing(false),
    finished(false),
    last_phoneme_samp(0),
    last_period_samp(0),
    transition_start(0)
{
    // generate hann window of size L
    for(int i = 0; i < L; i++)
    {
        window[i] = hann(L, i);
    }
}

Synthesizer::Synthesizer(std::vector<int> const& phonemes):
    Synthesizer()
{
    this->phonemes = phonemes;
}

// transition between phonemes
void Synthesizer::phoneme_transition()
{
    int ch = phonemes[phoneme_idx];
    int next = phonemes[phoneme_idx + 1];

    //printf("generating %d : %d..\n", phoneme_idx, ch);

    // find the sample file for diphone
    std::stringstream filename;
    filename << std::setfill('0') << std::setw(2) << ch << std::setw(2) << next;

    std::cout << filename.str() << std::endl;
    
    orig2 = get_samples_from_file(filename.str());
}

std::vector<float> Synthesizer::generate_frame()
{
    // shift the signal left to get space for the new signal
    sig = sig.shift(R);
    sig2 = sig2.shift(R);
    peaks = peaks.shift(R);
    
    // if current phoneme(orig) ended
    if(samp - last_phoneme_samp >= orig.size())
    {
        orig = orig2;
        sig = sig2;
        ova = ova2;
        last_phoneme_samp += transition_start;
    }
    
    // start of this frame in current phoneme, in samples
    unsigned frame_start = samp - last_phoneme_samp;
    
    // starting sample of the next phoneme in current phoneme
    transition_start = orig.size() / R * R - POv;

    // generate R samples
    for(int i = 0; i < R; i++)
    {
        unsigned progress_samp = frame_start + i;
        
        if(progress_samp == transition_start)
        {
            // if the synthesis has finished
            if((unsigned)++phoneme_idx >= phonemes.size() - 1)
            {
                finished = true;
                break;
            }
            
            phoneme_transition();
        }
        
        // append the signal at the end
        sig[L - R + i] = orig[progress_samp];
        
        if(progress_samp >= transition_start)
        {
            sig2[L - R + i] = orig2[progress_samp - transition_start];
        }

        // generate an impulse train
        fpoint freq = 200 * pow(1.2, sin(samp / 2000.0));
        fpoint period = Fs / freq;
        
        if(last_period_samp + period <= samp)
        {
            last_period_samp += period;
            peaks[L - R + i] = 1;
        }
        
        // increase global current sample index
        samp++;
    }
    
    // process generated R samples
    auto psola = [](rsig_t &sig, std::valarray<int> &peaks){
        // apply analysis window
        rsig_t seg = sig * window; // size : L
        
        csig_t ft(L); // size : L
        
        for(unsigned i = 0; i < L; i++) ft[i] = seg[i];
        
        fft(ft);
    
        // find fundamental period by autocorrelation
        int p = fundamental_period(ft);
        
        //DEBUG: print periods
        std::cout << p << ", ";
        
        // perform time domain pitch synchronous overlap add
        // divides seg into periods of size p
        // and matches it to peaks
        seg = td_psola(seg, peaks, p);
        
        return seg;
    };
    
    // overlap add
    ova = ova.shift(R) + psola(sig, peaks) * window * 0.3;
    ova2 = ova2.shift(R) + psola(sig2, peaks) * window * 0.3;
    
    // write output
    // take first R values of ova buffer, and clip to [-1,1]
    std::vector<float> out(R, 0);
    for(unsigned i = 0; i < R; i++)
    {
        // transition window(triangular) weight
        fpoint tr = linear(1, 0, frame_start + i, transition_start, orig.size());
        out[i] = (float)clamp(-1, 1, ova[i] * tr + ova2[i] * (1 - tr) );
    }

    return out;
}

bool Synthesizer::has_ended()
{
    return finished;
}
